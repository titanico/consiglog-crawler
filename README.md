# Instruções

As instruções a seguir são referentes às configurações e comandos básicos para desenvolvimento, teste e deploy da aplicação ConsigBot.

## Software necessário
	Anaconda
	Git
	GitKraken
	VSCode
	Python (Versão >= 3.7.4)

## Virtual Enviroment
É **obrigatório** que após ter instalado o Anaconda, via terminal, digite:

    conda create --name ConsigBot_Dev

Para a criação de um ambiente virtual python dedicado apenas ao projeto.

Utilize o seguinte comando para começar a usar o ambiente virtual:

    conda activate ConsigBot_Dev

O ambiente virá sem pacotes instalados, instale-os segundo o tópico **Instalações**.

## Instalações

	conda install pip
	pip install PyQt5
	pip install requests_html
	pip install beautifulsoup4
	pip install pypiwin32

 Caso haja ausência de pacotes após as instalações citadas, reporte-as ao gerente do projeto e acrescente-as neste tópico.

## Execução do projeto

Para executar o projeto via terminal de comando, na pasta raíz do mesmo, execute:
    
    python JanelaPrincipal.py

No VSCode execute através do menu:
    
    Debug > Start Debug

## Outras informações

### Branch de liberação ao Atendimento
	master

### Branch de produção(desenvolvimento)
	release

Crie *branches* a partir da *release* para quaisquer alterações de código. Siga o seguinte padrão:
    
    hotfix_[nome da alteração]
    fix_[nome da alteração]
    feature_[nome da alteração]

### Comando para gerar executável (.exe):

#### Geração de Bundle(OneDir):

    [Windows] pyinstaller --distpath C:\Users\[Seu usuário]\Deploys\ConsigBot\release_vX.Y.Z --icon designs\icones\icone_titan48x48.ico --version-file config\version_info.txt --onedir --windowed --win-private-assemblies JanelaPrincipal.py

#### Geração de executável único(OneFile):

    [Windows] pyinstaller --distpath C:\Users\[Seu usuário]\Deploys\ConsigBot\release_vX.Y.Z --icon designs\icones\icone_titan48x48.ico --version-file config\version_info.txt --onefile --windowed --win-private-assemblies JanelaPrincipal.py

### Links úteis:

[Windows] [Configurando terminal de python no VSCode](http://mscodingblog.blogspot.com/2017/08/setup-integrated-terminal-in-vs-code-to.html
):

    C:\Windows\System32\cmd.exe

[Windows/Linux] [Gerenciando ambiente virtuais com o Anaconda](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-with-commands)
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'janelaConsulta.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(440, 314)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout.addWidget(self.line, 1, 0, 1, 1)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_3.addWidget(self.label_3)
        self.label_con_atual = QtWidgets.QLabel(self.centralwidget)
        self.label_con_atual.setObjectName("label_con_atual")
        self.horizontalLayout_3.addWidget(self.label_con_atual)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_3.addWidget(self.label_4)
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_3.addWidget(self.label_5)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem1)
        self.verticalLayout_3.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem2)
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_5.addWidget(self.label_6)
        self.label_time = QtWidgets.QLabel(self.centralwidget)
        self.label_time.setObjectName("label_time")
        self.horizontalLayout_5.addWidget(self.label_time)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem3)
        self.verticalLayout_3.addLayout(self.horizontalLayout_5)
        self.progressBar = QtWidgets.QProgressBar(self.centralwidget)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName("progressBar")
        self.verticalLayout_3.addWidget(self.progressBar)
        self.gridLayout.addLayout(self.verticalLayout_3, 4, 0, 1, 1)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem4)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_4.addWidget(self.pushButton)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem5)
        self.gridLayout.addLayout(self.horizontalLayout_4, 5, 0, 1, 1)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_2.addWidget(self.label_2)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.lineEditCPF = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditCPF.setObjectName("lineEditCPF")
        self.horizontalLayout_2.addWidget(self.lineEditCPF)
        self.pushButtonCPFCarregar = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonCPFCarregar.setObjectName("pushButtonCPFCarregar")
        self.horizontalLayout_2.addWidget(self.pushButtonCPFCarregar)
        self.pushButtonCPFLimpar = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonCPFLimpar.setObjectName("pushButtonCPFLimpar")
        self.horizontalLayout_2.addWidget(self.pushButtonCPFLimpar)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)
        self.gridLayout.addLayout(self.verticalLayout_2, 2, 0, 1, 1)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.lineEditUsuario = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditUsuario.setObjectName("lineEditUsuario")
        self.horizontalLayout.addWidget(self.lineEditUsuario)
        self.pushButtonUsuarioCarregar = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonUsuarioCarregar.setObjectName("pushButtonUsuarioCarregar")
        self.horizontalLayout.addWidget(self.pushButtonUsuarioCarregar)
        self.pushButtonUsuarioLimpar = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonUsuarioLimpar.setObjectName("pushButtonUsuarioLimpar")
        self.horizontalLayout.addWidget(self.pushButtonUsuarioLimpar)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.gridLayout.addWidget(self.line_2, 3, 0, 1, 1)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        spacerItem6 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem6)
        self.pushButton_interrupt = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_interrupt.setObjectName("pushButton_interrupt")
        self.horizontalLayout_6.addWidget(self.pushButton_interrupt)
        spacerItem7 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem7)
        self.gridLayout.addLayout(self.horizontalLayout_6, 6, 0, 1, 1)
        self.horizontalLayout_7.addLayout(self.gridLayout)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 440, 22))
        self.menubar.setObjectName("menubar")
        self.menuArquivo = QtWidgets.QMenu(self.menubar)
        self.menuArquivo.setObjectName("menuArquivo")
        self.menuConsulta = QtWidgets.QMenu(self.menubar)
        self.menuConsulta.setObjectName("menuConsulta")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionAbrir_tabela_de_pessoa = QtWidgets.QAction(MainWindow)
        self.actionAbrir_tabela_de_pessoa.setObjectName("actionAbrir_tabela_de_pessoa")
        self.actionAbrir_tabela_de_CPFs = QtWidgets.QAction(MainWindow)
        self.actionAbrir_tabela_de_CPFs.setObjectName("actionAbrir_tabela_de_CPFs")
        self.actionSair = QtWidgets.QAction(MainWindow)
        self.actionSair.setObjectName("actionSair")
        self.actionInciar = QtWidgets.QAction(MainWindow)
        self.actionInciar.setObjectName("actionInciar")
        self.actionInterromper = QtWidgets.QAction(MainWindow)
        self.actionInterromper.setObjectName("actionInterromper")
        self.menuArquivo.addAction(self.actionAbrir_tabela_de_pessoa)
        self.menuArquivo.addAction(self.actionAbrir_tabela_de_CPFs)
        self.menuArquivo.addSeparator()
        self.menuArquivo.addAction(self.actionSair)
        self.menuConsulta.addAction(self.actionInciar)
        self.menuConsulta.addAction(self.actionInterromper)
        self.menubar.addAction(self.menuArquivo.menuAction())
        self.menubar.addAction(self.menuConsulta.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Consultas"))
        self.label_3.setText(_translate("MainWindow", "Progresso:"))
        self.label_con_atual.setText(_translate("MainWindow", "--"))
        self.label_4.setText(_translate("MainWindow", "/"))
        self.label_5.setText(_translate("MainWindow", "--"))
        self.label_6.setText(_translate("MainWindow", "Tempo Estimado: "))
        self.label_time.setText(_translate("MainWindow", "0:00:00"))
        self.pushButton.setText(_translate("MainWindow", "Consultar"))
        self.label_2.setText(_translate("MainWindow", "CPFs:"))
        self.lineEditCPF.setPlaceholderText(_translate("MainWindow", "Carregar arquivo de CPFs"))
        self.pushButtonCPFCarregar.setText(_translate("MainWindow", "Carregar"))
        self.pushButtonCPFLimpar.setText(_translate("MainWindow", "Limpar"))
        self.label.setText(_translate("MainWindow", "Logins/Senhas:"))
        self.lineEditUsuario.setPlaceholderText(_translate("MainWindow", "Carregar arquivo de Logins/Senhas"))
        self.pushButtonUsuarioCarregar.setText(_translate("MainWindow", "Carregar"))
        self.pushButtonUsuarioLimpar.setText(_translate("MainWindow", "Limpar"))
        self.pushButton_interrupt.setText(_translate("MainWindow", "Interromper"))
        self.menuArquivo.setTitle(_translate("MainWindow", "Arquivo"))
        self.menuConsulta.setTitle(_translate("MainWindow", "Consulta"))
        self.actionAbrir_tabela_de_pessoa.setText(_translate("MainWindow", "Abrir tabela de pessoa"))
        self.actionAbrir_tabela_de_pessoa.setShortcut(_translate("MainWindow", "Ctrl+O"))
        self.actionAbrir_tabela_de_CPFs.setText(_translate("MainWindow", "Abrir tabela de  CPFs"))
        self.actionAbrir_tabela_de_CPFs.setShortcut(_translate("MainWindow", "Ctrl+Alt+O"))
        self.actionSair.setText(_translate("MainWindow", "Sair"))
        self.actionInciar.setText(_translate("MainWindow", "Iniciar"))
        self.actionInciar.setShortcut(_translate("MainWindow", "Ctrl+P"))
        self.actionInterromper.setText(_translate("MainWindow", "Interromper"))
        self.actionInterromper.setShortcut(_translate("MainWindow", "Ctrl+I"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

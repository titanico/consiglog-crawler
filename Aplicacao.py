﻿from modules import utils
from Crawler import Crawler
from threading import Lock
from models.Pessoa import Pessoa
from models.Usuario import Usuario
from models.Resultado import Resultado
from modules.logger import registra_warn
from enums.TipoConvenio import TipoConvenio
from PyQt5.QtCore import QThread, pyqtSignal
from concurrent.futures import ThreadPoolExecutor
from modules import bussinessResultado as bResultado
from modules import bussinessConsiglog as bConsiglog
from urllib import request, error


class AplicacaoThread(QThread):
    signal_resultado_por_usuario = pyqtSignal('PyQt_PyObject')
    signal_consulta_total = pyqtSignal('PyQt_PyObject')
    signal_consulta_atual = pyqtSignal('PyQt_PyObject')
    signal_progressBar = pyqtSignal('PyQt_PyObject')
    signal_erro = pyqtSignal('PyQt_PyObject')

    def __init__(self, usuarios, pessoas):
        QThread.__init__(self)
        self.usuarios = usuarios
        self.pessoas = pessoas

        self.consulta_total = len(usuarios)*len(pessoas)
        self.consulta_atual = 0
        self.signal_consulta_atual.emit(self.consulta_atual)
        self.run = False

    def stop(self):
        self.run = False

    def run(self):
        self.run = True
        try:
            usuarios = self.usuarios
            pessoas = self.pessoas

            # self.verifica_conexao("https://govbahia.consiglog.com.br/")
            self.verifica_senhas(usuarios)

            registra_warn("Início da consulta para lista de {0} pessoas e {1} usuários.".format(len(pessoas), len(usuarios)))
            resultados_por_usuario = bResultado.inicializa_resultados_por_usuario(usuarios)
            lock = Lock()
            with ThreadPoolExecutor(max_workers=len(usuarios)) as executor:
                for usuario in usuarios:
                    executor.submit(self.gera_resultados, usuario, pessoas, resultados_por_usuario, lock)

            registra_warn("Consulta finalizada.")
            self.salva_resultados(resultados_por_usuario, False)

        except Exception as ex:
            self.signal_erro.emit(str(ex))
            self.run = False
            return

    def salva_resultados(self, resultados_por_usuario, parcial):
        comando = (resultados_por_usuario, parcial)
        self.signal_resultado_por_usuario.emit(comando)

    def gera_resultados(self, usuario, pessoas, resultados_por_usuario, lock):
        crawler = Crawler()
        self.signal_consulta_total.emit(self.consulta_total)
        resultados_por_pessoa = bResultado.inicializa_resultados_por_pessoa(pessoas)
        try:
            print("")
            print("========================================================================================================================")
            print("")
            print("Logando com o usuário " + str(usuario.login) + "...")
            print("")

            crawler.loga("https://govbahia.consiglog.com.br/Login.aspx", usuario.login, usuario.senha)
            
            print("========================================================================================================================")

            lista_convenios = [TipoConvenio.Saeb, TipoConvenio.Suprev]

            # TODO: Transformar rotina de salvar periodicamente em método
            percent = 0.20 # Salva a cada 20% completos de pessoas para cada usuário
            status  = 0
            goal = len(pessoas)
            milestone = percent * goal

            for pessoa in pessoas:         
                status = status + 1
                if(status >= milestone):
                    if(goal != milestone):
                        milestone = milestone + (percent * goal)
                        with lock:
                            resultados_por_usuario[usuario.login] = resultados_por_pessoa
                            self.salva_resultados(resultados_por_usuario, True)

                if(self.run == False):
                    print("Consulta interrompida para usuário {0}.".format(usuario.login))
                    with lock:
                        registra_warn("Consulta interrompida para usuário {0}.".format(usuario.login))
                    break

                self.consulta_atual = self.consulta_atual + 1
                self.signal_consulta_atual.emit(self.consulta_atual)
                self.signal_progressBar.emit(100*self.consulta_atual/self.consulta_total)
                
                for tipo_convenio in lista_convenios: 
                    try:
                        crawler.seleciona_convenio("https://govbahia.consiglog.com.br/LoginSelecao.aspx", tipo_convenio)
                        conteudo_pagina = crawler.consulta_margem("https://govbahia.consiglog.com.br/Margem/ConsultaMargem.aspx", pessoa.cpf)
                
                        lista_resultado = list()
                        lista_resultado = crawler.recupera_resultado(conteudo_pagina)

                        for resultado in lista_resultado:
                            resultado.cpf = pessoa.cpf
                            resultado.nome_banco = usuario.nome_banco
                            resultado.codigo_banco = usuario.codigo_banco
                            resultado.taxa_mensal =  utils.testa_sinal_porcentagem((usuario.taxa_mensal_banco).replace(".",","))

                            #TODO: Melhorar maneira de se transferir os telefones para o resultado
                            resultado.fone1 = pessoa.fone1
                            resultado.fone2 = pessoa.fone2
                            resultado.fone3 = pessoa.fone3
                            resultado.fone4 = pessoa.fone4
                            resultado.fone5 = pessoa.fone5

                            resultado = bResultado.retira_none(resultado)
                            resultado = bResultado.padroniza_matricula(resultado)
                            resultado = bResultado.retira_string_vazia(resultado)

                            if resultado.saldo_devedor_sem_taxa != '0,00':
                                resultado.saldo_devedor_com_taxa = bConsiglog.calcula_saldo_devedor_tx_banco(resultado.valor_parcela, resultado.qtd_parcelas_restantes, utils.converte_porcentagem_para_float(usuario.taxa_mensal_banco))

                            if(resultados_por_pessoa[pessoa.cpf] is None):
                                resultados_por_pessoa[pessoa.cpf] = list()

                            if(resultado.qtd_parcelas_total != 0 or resultado.margem_disponivel or resultado.margem_disponivel != "0,00"):
                                resultados_por_pessoa[pessoa.cpf].append(resultado)

                    except Exception as e:
                        print(str(e))
                        # with lock:
                        #     logger.registra_erro(e)

        except Exception as e:
            print(str(e))
            # with lock:
            #     logger.registra_erro(e)

        else:
            with lock:
                resultados_por_usuario[usuario.login] = resultados_por_pessoa
                
        finally:
            print("========================================================================================================================")
            print("Deslogando...{0}".format(str(usuario.login)))
            crawler.desloga("https://govbahia.consiglog.com.br/Margem/ConsultaMargem.aspx")
            print("Consulta finalizada para o usuário {0}".format(usuario.login))

    def verifica_senhas(self, usuarios):
        crawler = Crawler()
        lista_bloqueio = []
        existe_senha_invalida = False
        for usuario in usuarios:
            conteudo_pagina = crawler.loga("https://govbahia.consiglog.com.br/Login.aspx", usuario.login, usuario.senha)
            if (conteudo_pagina.text.find("Login e/ou senha inválidos.") != -1):
                lista_bloqueio.append({"login":usuario.login,"valido":False, "rasao":True})
                existe_senha_invalida = True
            elif (conteudo_pagina.text.find("Usuário bloqueado.") != -1):
                lista_bloqueio.append({"login":usuario.login,"valido":True,"bloqueado":True})
                existe_senha_invalida = True
            elif (conteudo_pagina.find("gvOrgao_imgEntrar_0") != []):
                lista_bloqueio.append({"login":usuario.login,"valido":True,"bloqueado":False})
                crawler.desloga("https://govbahia.consiglog.com.br/LoginSelecao.aspx")
            else:
                lista_bloqueio.append({"login":usuario.login,"valido":False,"rasao":False})
                existe_senha_invalida = True

        if existe_senha_invalida:
            error_msgs = []
            for item in lista_bloqueio:
                if not item["valido"]:
                    if not item["rasao"]:
                        error_msgs.append("Usuário {0} invalido por razão desconhecida.".format(item["login"]))
                    else:
                        error_msgs.append("Usuário {0} invalido.".format(item["login"]))
                else:
                    if item["bloqueado"]:
                        error_msgs.append("Usuário {0} bloqueado.".format(item["login"]))
                    else:
                        pass
                        #error_msgs.append("Usuário {0} valido.".format(item["login"]))
            error_string = ""
            for error in error_msgs:
                error_string = error_string + error + "\n"
            raise Exception(error_string)
    
    def verifica_conexao(self, url):
        try:
            request.urlopen(url, timeout=30)
        except error.URLError:
            raise Exception("Erro","Sem conexão com a internet!")
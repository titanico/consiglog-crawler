class Pessoa():

    def __init__(self, cpf = None, nome = None, data_nascimento = None,
                fone1= None, fone2 = None, fone3 = None, 
                fone4 = None, fone5 = None):
        self.colunas = ["NuCPF", "NmConvenio", "NmCliente", "DtNasc", "FONE1", 
                        "FONE2", "FONE3", "FONE4", "FONE5"]
        self.cpf = cpf
        self.nome = nome
        self.data_nascimento = data_nascimento
        self.fone1 = fone1
        self.fone2 = fone2
        self.fone3 = fone3
        self.fone4 = fone4
        self.fone5 = fone5
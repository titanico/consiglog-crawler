class Resultado():
    def __init__(self, contador=None, nome = None, cpf = None, matricula = None, margem_real = None, margem_disponivel = None,
                convenio = None, codigo_banco = None, nome_banco = None, qtd_parcelas_total = None, valor_parcela = None,
                qtd_parcelas_restantes = None, qtd_parcelas_pagas = None, saldo_devedor_sem_taxa = None,
                saldo_devedor_com_taxa = None, taxa_mensal = None, fone1= None, 
                fone2 = None, fone3 = None, fone4 = None, fone5 = None):
        self.colunas = ["Contador", "NmCliente", "Matricula", "NuCPF", "MG Total(R$)", "MG Disponível(R$)", "NmConvenio", 
                        "CdBanco", "NmBanco", "PMT TOT", "PMTS PG", "PMT REST", "VL PARCELA(R$)",
                        "SD sem Taxa(R$)", "SD com Taxa(R$)", "Taxa(%)", "FONE1", "FONE2",
                        "FONE3", "FONE4", "FONE5"]
        self.contador = contador
        self.nome = nome
        self.matricula = matricula
        self.cpf = cpf
        self.margem_real = margem_real
        self.margem_disponivel = margem_disponivel
        self.convenio = convenio
        self.codigo_banco = codigo_banco
        self.nome_banco = nome_banco
        self.valor_parcela = valor_parcela
        self.qtd_parcelas_total = qtd_parcelas_total
        self.qtd_parcelas_pagas = qtd_parcelas_pagas
        self.qtd_parcelas_restantes = qtd_parcelas_restantes
        self.saldo_devedor_sem_taxa = saldo_devedor_sem_taxa
        self.saldo_devedor_com_taxa = saldo_devedor_com_taxa
        self.taxa_mensal = taxa_mensal
        self.fone1 = fone1
        self.fone2 = fone2
        self.fone3 = fone3
        self.fone4 = fone4
        self.fone5 = fone5


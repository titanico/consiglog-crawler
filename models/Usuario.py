class Usuario():

    def __init__(self, login = None, senha = None, nome_banco=None, codigo_banco=None, taxa_mensal_banco=None):
        self.colunas = ["LOGIN", "SENHA", "CODIGO_BANCO", "NOME_BANCO", "TAXA_MENSAL_BANCO"]
        self.login = login
        self.senha = senha
        self.nome_banco = nome_banco
        self.codigo_banco = codigo_banco
        self.taxa_mensal_banco = taxa_mensal_banco

import modules.planilha as pl
from Aplicacao import AplicacaoThread
import modules.logger as logger
from PyQt5 import QtCore, QtGui, QtWidgets
from enums.TipoPlanilha import TipoPlanilha
from modules import bussinessConsiglog as bConsiglog
from modules.saveFile import saveOutputFileName
from modules.openFile import openFileNameDialog
from modules.planilha import le, escreve_resultados
from modules.bussinessResultado import converte_para_resultado_por_pessoa
from modules.popup import showDialog
from modules.utils import convert_time
from datetime import datetime as dt
from PyQt5.QtCore import pyqtSignal

class Consulta_MainWindow(QtWidgets.QWidget):
    #Default Signals
    signal_kill_thread = pyqtSignal('PyQt_PyObject')

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(440, 314)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout.addWidget(self.line, 1, 0, 1, 1)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_3.addWidget(self.label_3)
        self.label_con_atual = QtWidgets.QLabel(self.centralwidget)
        self.label_con_atual.setObjectName("label_con_atual")
        self.horizontalLayout_3.addWidget(self.label_con_atual)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_3.addWidget(self.label_4)
        self.label_total = QtWidgets.QLabel(self.centralwidget)
        self.label_total.setObjectName("label_total")
        self.horizontalLayout_3.addWidget(self.label_total)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem1)
        self.verticalLayout_3.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_total")
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem2)
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_5.addWidget(self.label_6)
        self.label_time = QtWidgets.QLabel(self.centralwidget)
        self.label_time.setObjectName("label_time")
        self.horizontalLayout_5.addWidget(self.label_time)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem3)
        self.verticalLayout_3.addLayout(self.horizontalLayout_5)
        self.progressBar = QtWidgets.QProgressBar(self.centralwidget)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName("progressBar")
        self.verticalLayout_3.addWidget(self.progressBar)
        self.gridLayout.addLayout(self.verticalLayout_3, 4, 0, 1, 1)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem4)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_4.addWidget(self.pushButton)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem5)
        self.gridLayout.addLayout(self.horizontalLayout_4, 5, 0, 1, 1)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_2.addWidget(self.label_2)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.lineEditCPF = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditCPF.setObjectName("lineEditCPF")
        self.horizontalLayout_2.addWidget(self.lineEditCPF)
        self.pushButtonCPFCarregar = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonCPFCarregar.setObjectName("pushButtonCPFCarregar")
        self.horizontalLayout_2.addWidget(self.pushButtonCPFCarregar)
        self.pushButtonCPFLimpar = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonCPFLimpar.setObjectName("pushButtonCPFLimpar")
        self.horizontalLayout_2.addWidget(self.pushButtonCPFLimpar)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)
        self.gridLayout.addLayout(self.verticalLayout_2, 2, 0, 1, 1)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.lineEditUsuario = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditUsuario.setObjectName("lineEditUsuario")
        self.horizontalLayout.addWidget(self.lineEditUsuario)
        self.pushButtonUsuarioCarregar = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonUsuarioCarregar.setObjectName("pushButtonUsuarioCarregar")
        self.horizontalLayout.addWidget(self.pushButtonUsuarioCarregar)
        self.pushButtonUsuarioLimpar = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonUsuarioLimpar.setObjectName("pushButtonUsuarioLimpar")
        self.horizontalLayout.addWidget(self.pushButtonUsuarioLimpar)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.gridLayout.addWidget(self.line_2, 3, 0, 1, 1)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        spacerItem6 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem6)
        self.pushButton_interrupt = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_interrupt.setObjectName("pushButton_interrupt")
        self.horizontalLayout_6.addWidget(self.pushButton_interrupt)
        spacerItem7 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem7)
        self.gridLayout.addLayout(self.horizontalLayout_6, 6, 0, 1, 1)
        self.horizontalLayout_7.addLayout(self.gridLayout)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 440, 22))
        self.menubar.setObjectName("menubar")
        self.menuArquivo = QtWidgets.QMenu(self.menubar)
        self.menuArquivo.setObjectName("menuArquivo")
        self.menuConsulta = QtWidgets.QMenu(self.menubar)
        self.menuConsulta.setObjectName("menuConsulta")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionAbrir_tabela_de_pessoa = QtWidgets.QAction(MainWindow)
        self.actionAbrir_tabela_de_pessoa.setObjectName("actionAbrir_tabela_de_pessoa")
        self.actionAbrir_tabela_de_CPFs = QtWidgets.QAction(MainWindow)
        self.actionAbrir_tabela_de_CPFs.setObjectName("actionAbrir_tabela_de_CPFs")
        self.actionSair = QtWidgets.QAction(MainWindow)
        self.actionSair.setObjectName("actionSair")
        self.actionInciar = QtWidgets.QAction(MainWindow)
        self.actionInciar.setObjectName("actionInciar")
        self.actionInterromper = QtWidgets.QAction(MainWindow)
        self.actionInterromper.setObjectName("actionInterromper")
        self.menuArquivo.addAction(self.actionAbrir_tabela_de_pessoa)
        self.menuArquivo.addAction(self.actionAbrir_tabela_de_CPFs)
        self.menuArquivo.addSeparator()
        self.menuArquivo.addAction(self.actionSair)
        self.menuConsulta.addAction(self.actionInciar)
        self.menuConsulta.addAction(self.actionInterromper)
        self.menubar.addAction(self.menuArquivo.menuAction())
        self.menubar.addAction(self.menuConsulta.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Consultas"))
        self.label_3.setText(_translate("MainWindow", "Progresso:"))
        self.label_con_atual.setText(_translate("MainWindow", "--"))
        self.label_4.setText(_translate("MainWindow", "/"))
        self.label_total.setText(_translate("MainWindow", "--"))
        self.label_6.setText(_translate("MainWindow", "Tempo Estimado: "))
        self.label_time.setText(_translate("MainWindow", "0:00:00"))
        self.pushButton.setText(_translate("MainWindow", "Consultar"))
        self.label_2.setText(_translate("MainWindow", "CPFs:"))
        self.lineEditCPF.setPlaceholderText(_translate("MainWindow", "Carregar arquivo de CPFs"))
        self.pushButtonCPFCarregar.setText(_translate("MainWindow", "Carregar"))
        self.pushButtonCPFLimpar.setText(_translate("MainWindow", "Limpar"))
        self.label.setText(_translate("MainWindow", "Logins/Senhas:"))
        self.lineEditUsuario.setPlaceholderText(_translate("MainWindow", "Carregar arquivo de Logins/Senhas"))
        self.pushButtonUsuarioCarregar.setText(_translate("MainWindow", "Carregar"))
        self.pushButtonUsuarioLimpar.setText(_translate("MainWindow", "Limpar"))
        self.pushButton_interrupt.setText(_translate("MainWindow", "Interromper"))
        self.menuArquivo.setTitle(_translate("MainWindow", "Arquivo"))
        self.menuConsulta.setTitle(_translate("MainWindow", "Consulta"))
        self.actionAbrir_tabela_de_pessoa.setText(_translate("MainWindow", "Abrir tabela de pessoa"))
        self.actionAbrir_tabela_de_pessoa.setShortcut(_translate("MainWindow", "Ctrl+O"))
        self.actionAbrir_tabela_de_CPFs.setText(_translate("MainWindow", "Abrir tabela de  CPFs"))
        self.actionAbrir_tabela_de_CPFs.setShortcut(_translate("MainWindow", "Ctrl+Alt+O"))
        self.actionSair.setText(_translate("MainWindow", "Sair"))
        self.actionInciar.setText(_translate("MainWindow", "Iniciar"))
        self.actionInciar.setShortcut(_translate("MainWindow", "Ctrl+P"))
        self.actionInterromper.setText(_translate("MainWindow", "Interromper"))
        self.actionInterromper.setShortcut(_translate("MainWindow", "Ctrl+I"))

        ### Custom ###
        self.initAttributes()
        self.setSignalsSlots()

        self.signals_on = False
        
    def initAttributes(self):
        self.file_userSenha = ''
        self.file_cpf = ''
        self.pushButton_interrupt.setEnabled(False)

    def setSignalsSlots(self):
        #Botões
        self.pushButtonUsuarioCarregar.clicked.connect(lambda: self.getFilePath(TipoPlanilha.Usuario, "Abrir arquivo de usuários e senhas"))
        self.pushButtonUsuarioLimpar.clicked.connect(lambda: self.clearFilePath(TipoPlanilha.Usuario))
    
        self.pushButtonCPFCarregar.clicked.connect(lambda: self.getFilePath(TipoPlanilha.Pessoa, "Abrir arquivo de CPFs"))
        self.pushButtonCPFLimpar.clicked.connect(lambda: self.clearFilePath(TipoPlanilha.Pessoa))

        self.pushButton.clicked.connect(lambda: self.startQuery())
        self.pushButton_interrupt.clicked.connect(lambda: self.endQuery())

        #Actions
        self.actionAbrir_tabela_de_CPFs.triggered.connect(lambda: self.getFilePath(TipoPlanilha.Pessoa, "Abrir arquivo de CPFs"))
        self.actionAbrir_tabela_de_pessoa.triggered.connect(lambda: self.getFilePath(TipoPlanilha.Usuario, "Abrir arquivo de usuários e senhas"))
        self.actionInciar.triggered.connect(lambda: self.startQuery())
        self.actionInterromper.triggered.connect(lambda: self.endQuery())


    def updateTime(self):
        self.time_curr = dt.now()
        time_seg = self.time_curr - self.time_init
        time_seg = time_seg.total_seconds()
        time_per_query = time_seg/self.atual
        time_expected = convert_time((self.total-self.atual)*time_per_query)
        self.label_time.setText(str(time_expected))

    def updateQueryTotal(self, val):
        self.total = val
        self.label_total.setText(str(val))

    def updateQueryCurrent(self, val):
        self.atual = val
        self.label_con_atual.setText(str(val))
        self.updateTime()

    def updateProgressBar(self, val):
        self.progressBar.setValue(int(val))

    def buttonsSetEnable(self, boolean):
        self.pushButton.setEnabled(boolean)
        self.pushButtonCPFCarregar.setEnabled(boolean)
        self.pushButtonCPFLimpar.setEnabled(boolean)
        self.pushButtonUsuarioCarregar.setEnabled(boolean)
        self.pushButtonUsuarioLimpar.setEnabled(boolean)

        self.actionInciar.setEnabled(boolean)
        self.actionAbrir_tabela_de_CPFs.setEnabled(boolean)
        self.actionAbrir_tabela_de_pessoa.setEnabled(boolean)
        self.pushButton_interrupt.setEnabled(not boolean)

    def getFilePath(self, id, title):
        """
        Método para carregar os diretórios dos arquivos nos atributos corretos.
        O parâmetro id determina se qual arquivo está sendo aberto:
          TipoPlanilha.Usuario - .csv de usuários/senhas
          TipoPlanilha.Pessoa - .csv de cpfs
        O título deve informar qual arquivo está sendo buscado.

        O diretório do arquivo será exibido em sua respectiva lineEdit.

        Obs: Nenhum arquivo é aberto aqui.
             A abertura e leitura de arquivo é feita no método de consulta.

        Não há retorno.
        """

        file_var = openFileNameDialog(self, title)

        if(file_var):
            if(id == TipoPlanilha.Usuario):
                self.file_userSenha = file_var[0] #Obs: O primeiro indice é o diretório do arquivo. O segundo é o tipo.
                self.lineEditUsuario.setText(str(file_var[0]))
            elif(id == TipoPlanilha.Pessoa):
                self.file_cpf = file_var[0]
                self.lineEditCPF.setText(str(file_var[0]))
            else:
                print("id de arquivo inválido.")

    def clearFilePath(self, id):
        """
        Método para limpar o arquivo e o seu respectivo lineEdit
        """
        if(id == TipoPlanilha.Usuario):
            self.file_userSenha = ''
            self.lineEditUsuario.clear()
        elif(id == TipoPlanilha.Pessoa):
            self.file_cpf = ''
            self.lineEditCPF.clear()
        else:
            print("id de arquivo inválido.")

    def startQuery(self):
        """
        Método para iniciar a consulta.
        Passa os caminhos dos arquivos para o módulo planilha:
        A consulta retorna uma string que é escrita no arquivo
        no formato .txt criado pelo usuário.
        """

        self.time_init = dt.now()
        self.time_curr = None

        #Checar se há arquivos abertos.
        if(self.file_userSenha == '' ):
            title = "Não foi possível executar a consulta."
            msg = "Planilha de usuários/senhas não carregada."
            logger.registra_erro(msg)
            showDialog(title, msg)
            return

        if(self.file_cpf == ''):
            title = "Não foi possível executar a consulta."
            msg = "Planilha de pessoas não carregada."
            logger.registra_erro(msg)
            showDialog(title, msg)
            return

        if(not bConsiglog.verifica_horario_funcionamento()):
            msg = "O horário de funcionamento é entre as 7h00min e 20h00min."
            showDialog("Sistema fechado!", msg)
            return

        usuarios = le(self.file_userSenha, TipoPlanilha.Usuario)
        pessoas = le(self.file_cpf, TipoPlanilha.Pessoa)

        if(usuarios and pessoas):
            self.file_dir = saveOutputFileName(self)
            if(self.file_dir == None or not self.file_dir.strip()):
                msg = "Falha ao criar arquivo de saída: caminho não definido para o arquivo."
                logger.registra_erro(msg)
                showDialog("Um erro ocorreu!", msg)
                return None

            self.buttonsSetEnable(False)

            #Criação de QThread, conexão dos sinais para atualização de dados e tela.
            self.thread = AplicacaoThread(usuarios, pessoas)
            self.thread.signal_resultado_por_usuario.connect(self.saveResult)
            self.thread.signal_progressBar.connect(self.updateProgressBar)
            self.thread.signal_consulta_total.connect(self.updateQueryTotal)
            self.thread.signal_erro.connect(self.exibe_janela_erro)
            self.thread.signal_consulta_atual.connect(self.updateQueryCurrent)
            self.signal_kill_thread.connect(self.thread.stop)
            self.thread.start()

    def endQuery(self):
        if(self.thread):
            try:
                self.signal_kill_thread.emit("a")
                print("Thread finalizada")
            except:
                print("erro ao finalizar thread.")
                #tratar depois
        else:
            pass
        print("Apertou interrupt.")
        self.buttonsSetEnable(True)
        self.progressBar.setValue(0)
        self.label_total.setText("--")
        self.label_con_atual.setText("--")

    def exibe_janela_erro(self, erro_msg):
        showDialog("Erro", erro_msg)
        self.buttonsSetEnable(True)

    def saveResult(self, comando):
        print("Iniciando escrita...")
        resultados_por_usuario = comando[0]
        parcial = comando[1]
        resultados_por_pessoa = converte_para_resultado_por_pessoa(resultados_por_usuario)
        if(not parcial):
            self.buttonsSetEnable(True)
            self.progressBar.setValue(0)
            self.label_total.setText("--")
            self.label_con_atual.setText("--")

            pl.escreve_resultados(self.file_dir, resultados_por_pessoa)
            segs = (self.time_curr - self.time_init).total_seconds()
            showDialog("Sucesso!", "Consulta concluída em " + str(convert_time(segs)))
        else:
            pl.escreve_resultados(self.file_dir, resultados_por_pessoa)
        print("Finalizando escrita...")

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Consulta_MainWindow()
    ui.setupUi(MainWindow)
    app.aboutToQuit.connect(ui.endQuery)
    MainWindow.show()
    sys.exit(app.exec_())

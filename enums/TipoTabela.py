from enum import Enum

class TipoTabela(Enum):
    S_Emprestimo = 1
    Um_Emprestimo = 2
    Dois_Emprestimos = 3
from enum import Enum

class TipoPlanilha(Enum):
    Usuario = 0
    Pessoa = 1
    Resultado = 2
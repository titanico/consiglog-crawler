import csv

path = "test/usuario.csv" #colocar nome do arquivo

reader = list(csv.reader(open(path, "rU"), delimiter=',')) #abre o arquivo com delimiter ","
writer = csv.writer(open(path, 'w'), delimiter=';')
writer.writerows(row for row in reader)
from datetime import date, datetime

def registra_erro(erro):
        """
        Registra erros da aplicação em um arquivo de log específico. Recebe como
        parâmetro objetos derivados da classe Exception
        """
        nome_arquivo = "Log_{0}_{1}_{2}.txt".format(str(date.today().day),str(date.today().month).zfill(2),str(date.today().year))
        log = open(nome_arquivo, "a")
        log.write("[ERRO] " + str(erro)  + "[Ocorrido em " + str(datetime.now()) + "]" +  "\n")
        log.close()

def registra_warn(warn):
        """
        Registra alertas da aplicação em um arquivo de log específico. Recebe como
        parâmetro uma string
        """
        nome_arquivo = "Log_{0}_{1}_{2}.txt".format(str(date.today().day),str(date.today().month).zfill(2),str(date.today().year))
        log = open(nome_arquivo, "a")
        log.write("[WARN] " + warn  + "[Ocorrido em " + str(datetime.now()) + "]" +  "\n")
        log.close()

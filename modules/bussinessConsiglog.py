from datetime import datetime, date
from enums.DiaSemana import DiaSemana

#TODO: Verificar horário de funcionamento a partir de output do site e não mais por hora atual
def verifica_horario_funcionamento():
	hora = datetime.now().hour

	if hora < 7 or hora > 19:
		raise Exception("O horário de funcionamento é compreendido entre 7h00min e 20h00min.")
		return False
	else:
		return True

def verifica_dia_semana_funcionamento():
	dia_semana = DiaSemana(date.today().weekday())
	if(dia_semana == DiaSemana.Sabado or dia_semana == DiaSemana.Domingo):
		raise Exception("O sistema da SAEB não funciona aos finais de semana, seus resultados não serão exibidos.")

def calcula_saldo_devedor(valor_parcelas, parcelas_restantes):
	if (parcelas_restantes is None) or (valor_parcelas is None):
		return ''
	resultado = parcelas_restantes * valor_parcelas
	return round(resultado, 2)

def calcula_saldo_devedor_tx_banco(valor_parcelas, parcela_restantes, taxa_banco):
	if (parcela_restantes is None) or (valor_parcelas is None):
		return ''
	resultado = valor_parcelas * ((1 + taxa_banco)**(parcela_restantes) - 1)/((1 + taxa_banco)**(parcela_restantes + 1) - (1 + taxa_banco)**(parcela_restantes))
	return round(resultado, 2)
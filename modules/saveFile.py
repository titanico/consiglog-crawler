from PyQt5.QtWidgets import QFileDialog as fd
from PyQt5.QtWidgets import QApplication, QWidget, QInputDialog, QLineEdit
from PyQt5.QtGui import QIcon
import sys, csv

def saveFileNameDialog(mainWindow, title):
    # Recebe como parâmetro uma MainWindow.
    # Obs: O dialog só pode ser construído a partir de uma janela pai.
    #
    # Retorna o diretório do arquivo.

    fileName = fd.getSaveFileName(mainWindow, title, "", "Csv (*.csv)")

    if (fileName):
        print(fileName)
        return fileName
    else:
        print("Nenhum arquivo foi salvo.")
        return None

def saveOutputFileName(window):
    return saveFileNameDialog(window, "Salvar arquivo de saída")[0]

class App(QWidget):
    # Classe arbitraria de teste.
    # Remover depois de posteriores testes.

    def __init__(self):
        super().__init__()
        self.title = 'teste'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.initUI()
    
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        saveFileNameDialog(self, 'teeste titanci')
        
        self.show()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
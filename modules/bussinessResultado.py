from models.Resultado import Resultado
from models.Pessoa import Pessoa
from modules.utils import merge_dicionarios_de_listas

def retira_none(resultado):
	if resultado.qtd_parcelas_total == None:
		resultado.qtd_parcelas_total = ""
	if resultado.qtd_parcelas_pagas == None:
		resultado.qtd_parcelas_pagas = ""
	if resultado.qtd_parcelas_restantes == None:
		resultado.qtd_parcelas_restantes = ""
	if resultado.saldo_devedor_sem_taxa == None:
		resultado.saldo_devedor_sem_taxa = ""
	if resultado.saldo_devedor_com_taxa == None:
		resultado.saldo_devedor_com_taxa = ""
	if resultado.taxa_mensal == None:
		resultado.taxa_mensal = ""
	return resultado

def padroniza_matricula(resultado):
	resultado.matricula = resultado.matricula[:-4]
	numeros_restantes = len(resultado.matricula)
	resultado.matricula = resultado.matricula[-(numeros_restantes-2):]
	return resultado

def inicializa_resultados_por_pessoa(pessoas):
    """
    Recebe uma lista de Pessoas e retorna um dicionário
    cujas chaves são os CPF's.
    """
    cpfs = list()
    for pessoa in pessoas:
	    cpfs.append(pessoa.cpf)
    resultados_por_pessoa = dict.fromkeys(cpfs)
    return resultados_por_pessoa

def inicializa_resultados_por_usuario(usuarios):
    """
    Recebe uma lista de Usuarios e retorna um dicionário
    cujas chaves são login's  dos usuários.
    """
    logins = list()
    for usuario in usuarios:
	    logins.append(usuario.login)

    resultados_por_usuario = dict.fromkeys(logins)

    for usuario in usuarios:
        resultados_por_usuario[usuario.login] = dict()
    return resultados_por_usuario

def converte_para_resultado_por_pessoa(resultado_por_usuario):
    """
    Recebe um dicionário cujas chaves são os logins dos usuários e os valores
    dicionários(Key = Pessoa.cpf, Value = list(Resultado)) e converte para um 
    dicionário cujas chaves são os CPF's das pessoas e os valores são listas 
    de Resultados vinculados a pessoa.
    """
    usuarios = list(resultado_por_usuario.keys())
    resultados_por_pessoa = resultado_por_usuario[usuarios.pop()]
    for usuario in usuarios:
        resultados_por_pessoa = merge_dicionarios_de_listas(resultados_por_pessoa, resultado_por_usuario[usuario])
    
    return resultados_por_pessoa

def retira_string_vazia(resultado):

    if resultado.qtd_parcelas_pagas == "":
        resultado.qtd_parcelas_pagas = "0"
    if resultado.qtd_parcelas_restantes == "":
        resultado.qtd_parcelas_restantes = "0"
    if resultado.qtd_parcelas_total == "":
        resultado.qtd_parcelas_total = "0"
    if resultado.saldo_devedor_com_taxa == "":
        resultado.saldo_devedor_com_taxa = "0,00"
    if resultado.saldo_devedor_sem_taxa == "":
        resultado.saldo_devedor_sem_taxa = "0,00"
    if resultado.valor_parcela == "":
        resultado.valor_parcela = "0,00"

    return resultado



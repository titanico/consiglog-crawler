import csv
from modules.popup import showDialog
import modules.logger as logger
from models.Pessoa import Pessoa
from models.Usuario import Usuario
from models.Resultado import Resultado
from enums.TipoPlanilha import TipoPlanilha
from modules.utils import converte_float_para_string, converte_string_para_float, valida_cpf, converte_porcentagem_para_float, testa_erro_taxa

def le(url_arquivo_entrada, tipo_planilha):
    """
    Recebe o caminho para um arquivo csv como entrada e o tipo de planilha(TipoPlanilha) a ser lida.
    Retorna uma lista de Resultado/Pessoa/Usuario ou Banco. Cada item retornado tem as colunas da
    planilha CSV e seus respectivos valores
    """
    try:
        arquivo_entrada = open(url_arquivo_entrada, mode='r', encoding="iso-8859-1")
        planilha_reader = csv.DictReader(arquivo_entrada, delimiter=";")
        lista = list()  
    except Exception as e:
        msg = "O seguinte erro ocorreu ao se tentar ler o arquivo '{0}': {1}".format(url_arquivo_entrada, e)
        logger.registra_erro(msg)
        showDialog("Um erro ocorreu!", msg)
        return None

    if tipo_planilha == TipoPlanilha.Usuario:
        print("Carregada planilha de usuarios/senhas.")
        usuario = Usuario()
        for linha in planilha_reader:
            try:
                usuario.login = str(linha["LOGIN"])
                usuario.senha = str(linha["SENHA"])
                usuario.codigo_banco = str(linha["CODIGO_BANCO"])
                usuario.nome_banco = str(linha["NOME_BANCO"])
                usuario.taxa_mensal_banco = str(linha["TAXA_MENSAL_BANCO"]) #str(converte_string_para_float(linha["TAXA_MENSAL_BANCO"]))
                testa_erro_taxa(usuario.taxa_mensal_banco)
                lista.append(usuario)
                usuario = Usuario()

            except Exception as e:
                msg = ("Erro no modelo de planilha. Coluna ausente: {0}".format(e))
                logger.registra_erro(msg)
                showDialog("Um erro ocorreu!", msg)
                return None
        
    elif tipo_planilha == TipoPlanilha.Pessoa:
        print("carregada planilha de cpf.")    
        pessoa = Pessoa()
        count = 1
        for linha in planilha_reader:
            try:
                count = count + 1
                if valida_cpf(str(linha["NuCPF"])):
                    pessoa.cpf = (str(linha["NuCPF"])).zfill(11)
                    pessoa.nome = (str(linha["NmCliente"]))
                    pessoa.data_nascimento = (str(linha["DtNasc"]))
                    pessoa.fone1 = (str(linha["FONE1"]))
                    pessoa.fone2 = (str(linha["FONE2"]))
                    pessoa.fone3 = (str(linha["FONE3"]))
                    pessoa.fone4 = (str(linha["FONE4"]))
                    pessoa.fone5 = (str(linha["FONE5"]))

                    lista.append(pessoa)
                    pessoa = Pessoa()
                else:
                    msg = "CPF {0} inválido.".format(str(linha["NuCPF"]))
                    logger.registra_erro("No arquivo {0} para linha {1} houve o seguinte erro: {2}".format(url_arquivo_entrada, count, msg))
            except Exception as e:
                msg = ("Erro no modelo de planilha. Coluna ausente: {0}".format(e))
                logger.registra_erro(msg)
                showDialog("Um erro ocorreu!", msg)
                return None

    return lista

def escreve_resultados(url_arquivo_saida, resultados_por_pessoa):
    """
    Recebe o caminho para o arquivo csv que se deseja escrever e um dicionário de listas de Resultado
    representando a planilha lida.
    """
    try:
        arquivo_saida = open(url_arquivo_saida, mode='w', newline='')

        if(len(resultados_por_pessoa) > 0):
            writer = csv.DictWriter(arquivo_saida, fieldnames=Resultado().colunas, quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
            writer.writeheader()
        
        contador = 1
        for lista_resultados in resultados_por_pessoa.values():
            if(lista_resultados):
                for resultado in lista_resultados:
                    aux_resultado = dict()
                    if(lista_resultados.index(resultado) == 0):
                        aux_resultado["Contador"] = str(contador)
                    else:
                        aux_resultado["Contador"] = "#"

                    aux_resultado["NmCliente"] = resultado.nome
                    aux_resultado["Matricula"] = resultado.matricula
                    aux_resultado["NuCPF"] = str(resultado.cpf)

                    aux_resultado["NmConvenio"] = resultado.convenio
                    aux_resultado["CdBanco"] = resultado.codigo_banco
                    aux_resultado["NmBanco"] = resultado.nome_banco

                    aux_resultado["MG Total(R$)"] = converte_float_para_string(resultado.margem_real)
                    aux_resultado["MG Disponível(R$)"] = converte_float_para_string(resultado.margem_disponivel)
                    aux_resultado["PMT TOT"] = str(resultado.qtd_parcelas_total)
                    aux_resultado["PMTS PG"] = str(resultado.qtd_parcelas_pagas)
                    aux_resultado["PMT REST"] = str(resultado.qtd_parcelas_restantes) 
                    aux_resultado["VL PARCELA(R$)"] = converte_float_para_string(resultado.valor_parcela)
                    aux_resultado["Taxa(%)"] = resultado.taxa_mensal
                    aux_resultado["SD sem Taxa(R$)"] = converte_float_para_string(resultado.saldo_devedor_sem_taxa)
                    aux_resultado["SD com Taxa(R$)"] = converte_float_para_string(resultado.saldo_devedor_com_taxa)

                    aux_resultado["FONE1"] = resultado.fone1
                    aux_resultado["FONE2"] = resultado.fone2
                    aux_resultado["FONE3"] = resultado.fone3
                    aux_resultado["FONE4"] = resultado.fone4
                    aux_resultado["FONE5"] = resultado.fone5
                    writer.writerow(aux_resultado)
                contador = contador + 1
                
    except Exception as e:
        msg = "O seguinte erro ocorreu ao se tentar escrever o arquivo: {0}".format(e)
        showDialog("Um erro ocorreu!", msg)
        logger.registra_erro(msg)

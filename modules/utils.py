from modules.logger import registra_erro
from modules.popup import showDialog
import csv

def convert_time(seconds):
    seconds = seconds % (24 * 3600) 
    hour = seconds/3600
    seconds %= 3600
    minutes = seconds/60
    seconds %= 60
      
    return "%d:%02d:%02d" % (hour, minutes, seconds) 

def valida_cpf(cpf):
	cpf = cpf.zfill(11)
	primeiro_num = cpf[0]
	if(primeiro_num*11 !=  cpf):
		count = 10
		soma = 0
		while count != 1:
			soma = soma + int(cpf[10-count])*count
			count = count - 1
		resto = (soma * 10) % 11
		if resto == 10:
			resto = 0
		if resto == int(cpf[9]):
			count = 11
			soma = 0
			while count != 1:
				soma = soma + int(cpf[11-count])*count
				count = count - 1
			resto = (soma * 10) % 11
			if resto == 10:
				resto = 0
			if  resto == int(cpf[10]):
				return True
	return False

def converte_string_para_float(numero):
	if str(type(numero)) == "<class 'int'>":
		return numero
	else:
	    try:
		    numero = numero.replace('.', '')  #retira os pontos
		    numero = numero.replace(',', '.') #substitui virgula por ponto
		    numero = float(numero)            #converte para float
		    return numero
	    except ValueError:
		    msg = ("Não foi possível converter '{0}' para decimal.".format(numero))
		    raise Exception(msg)
	
def converte_float_para_string(numero):
	if str(type(numero)) == "<class 'str'>":
		return numero
	else:
		try:
				numero = format(numero, '.2f').replace('.', ',')	#substitui ponto por vírgula
				numero = str(numero)					#converte para string e coloca entre aspas
				return numero
		except ValueError:
			msg = ("Não foi possível converter '{0}' para string.".format(numero))
			raise Exception(msg)	

def merge_dicionarios_de_listas(dict1, dict2):
	dict_resultado = {**dict1, **dict2}
	for key in dict_resultado.keys():
		if key in dict1 and key in dict2:
			if(dict1[key]) and (dict_resultado[key]):
				dict_resultado[key] = dict_resultado[key] + dict1[key]
			elif (dict1[key]) and (not dict_resultado[key]):
				dict_resultado[key] = dict1[key]
	return dict_resultado

def converte_porcentagem_para_float(numero):
	try:
		numero = numero.replace('%','') #retira o sinal %
		numero = numero.replace(',', '.') #substitui virgula por ponto
		numero = float(numero)            #converte para float
		numero = numero/100 #divide por 100 (por ser porcentagem)
		return numero
	except ValueError:
		msg = ("Não foi possível converter '{0}' para decimal.".format(numero))
		raise Exception(msg)	

def testa_sinal_porcentagem(valor):
	if valor.find("%") == -1:
		valor = valor + "%"
		return valor
	else:
		return valor

def testa_erro_taxa(taxa):
	try: 
		converte_porcentagem_para_float(taxa)
	except ValueError:
		msg = "Não foi possível converter '{0}' para decimal.".format(taxa)
		registra_erro(msg)
		showDialog("Não foi possível executar a consulta!",msg)
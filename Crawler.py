import time
import codecs
from re import compile
from modules import utils
from datetime import datetime
from bs4 import BeautifulSoup as bs
from requests_html import HTMLSession
from models.Resultado import Resultado
from enums.TipoTabela import TipoTabela
from enums.TipoConvenio import TipoConvenio
from modules import bussinessConsiglog as bConsiglog

class Crawler:

    def __init__(self):
        self.sessao = HTMLSession()

    def loga(self, url, login, senha):
        
        
        dados = {"txtLogin":login, "txtMatricula":"", "txtSenha":senha, "txtCaptcha":"", "Entrar":"Entrar", "__LASTFOCUS":"", "__EVENTTARGET":"", "__EVENTARGUMENT":"", "__VIEWSTATE":"", "__VIEWSTATEGENERATOR":""}
        html_pagina = self.sessao.post(url, dados)
        conteudo_pagina = bs(html_pagina.html.html, "html.parser")

        viewstate = conteudo_pagina.find("input", {"id":"__VIEWSTATE"})["value"]
        viewstate_generator = conteudo_pagina.find("input", {"id":"__VIEWSTATEGENERATOR"})["value"]

        dados = {"txtLogin":login, "txtMatricula":"", "txtSenha":senha, "txtCaptcha":"", "Entrar":"Entrar", "__LASTFOCUS":"", "__EVENTTARGET":"", "__EVENTARGUMENT":"", "__VIEWSTATE":viewstate, "__VIEWSTATEGENERATOR":viewstate_generator}
        html_pagina = self.sessao.post(url, dados)
        
        conteudo_pagina = bs(html_pagina.html.html, "html.parser")

        janela_popup = conteudo_pagina.find(id="ucAjaxModalPopupConfirmacao1_PanelPopup")
        if janela_popup is not None:
            span_janela_popup = janela_popup.find(id="ucAjaxModalPopupConfirmacao1_lblMensagemPopup")
            mensagem_deslogar = "Usuário já logado, deseja desconectar seu usuário dos outros terminais?" 
            if (span_janela_popup.text == mensagem_deslogar):
                self.desloga_outro_usuario_logado(url, dados, conteudo_pagina)
                html_pagina = self.sessao.post("https://govbahia.consiglog.com.br/LoginSelecao.aspx")
                conteudo_pagina = bs(html_pagina.html.html, "html.parser")

        return conteudo_pagina

    def desloga_outro_usuario_logado(self, url, dados, conteudo_pagina):
        '''
        Desloga qualquer outro usuário que esteja logado no sistema para que o usuário atual
        possa logar. Recebe a url do login, um JSON com os dados para a requisição POST e 
        conteudo_pagina que é um BeautifulSoup da página com mensagem de deslogar outro usuário.
        '''
        botao_desconecta = conteudo_pagina.find(id="ucAjaxModalPopupConfirmacao1_btnConfirmarPopup")

        # Caso o botão de aviso para deslogar outro usuário não exista, é porque
        # o login foi um sucesso e pulamos o if
        if (botao_desconecta != None and  len(botao_desconecta) > 1):
            name_botao_desconecta_outro_usuario = botao_desconecta[1]["name"]
            value_botao_desconecta_outro_usuario = botao_desconecta[1]["value"]

            dados[name_botao_desconecta_outro_usuario] = value_botao_desconecta_outro_usuario
            self.sessao.post(url, dados)

    def seleciona_convenio(self, url, tipo_conv):
        tipo_conv = TipoConvenio(tipo_conv)

        tem_convenio_dpeba = False
        tem_convenio_dpeba_e_tjba = False

        try:
            html_pagina = self.sessao.get(url)
            conteudo_pagina = bs(html_pagina.html.html, "html.parser")
            name_botao_seleciona = conteudo_pagina.find(id=compile("imgEntrar_2$"))["name"]
            tem_convenio_dpeba = True
            try:
                name_botao_seleciona = conteudo_pagina.find(id=compile("imgEntrar_3$"))["name"]
                tem_convenio_dpeba = False
                tem_convenio_dpeba_e_tjba = True
            except:
                pass
        except:
            pass

        if tem_convenio_dpeba:
            if tipo_conv == TipoConvenio.Saeb:
                bConsiglog.verifica_dia_semana_funcionamento()
                html_pagina = self.sessao.get(url)
                conteudo_pagina = bs(html_pagina.html.html, "html.parser")

                viewstate = conteudo_pagina.find("input", {"id":"__VIEWSTATE"})["value"]
                viewstate_generator = conteudo_pagina.find("input", {"id":"__VIEWSTATEGENERATOR"})["value"]

                posicao_botao_seleciona_x = 16
                posicao_botao_seleciona_y = 9

                name_botao_seleciona = conteudo_pagina.find(id=compile("imgEntrar_1$"))["name"]
                dados = {"__EVENTTARGET":"", "__EVENTARGUMENT":"", "__VIEWSTATE":viewstate, "__VIEWSTATEGENERATOR":viewstate_generator, name_botao_seleciona+".x":posicao_botao_seleciona_x, name_botao_seleciona+".y":posicao_botao_seleciona_y}
                self.sessao.post(url, dados)

            if tipo_conv == TipoConvenio.Suprev:
                html_pagina = self.sessao.get(url) 
                conteudo_pagina = bs(html_pagina.html.html, "html.parser")

                viewstate = conteudo_pagina.find("input", {"id":"__VIEWSTATE"})["value"]
                viewstate_generator = conteudo_pagina.find("input", {"id":"__VIEWSTATEGENERATOR"})["value"]

                posicao_botao_seleciona_x = 5
                posicao_botao_seleciona_y = 7

                name_botao_seleciona = conteudo_pagina.find(id=compile("imgEntrar_2$"))["name"]
                dados = {"__EVENTTARGET":"", "__EVENTARGUMENT":"", "__VIEWSTATE":viewstate, "__VIEWSTATEGENERATOR":viewstate_generator, name_botao_seleciona+".x":posicao_botao_seleciona_x, name_botao_seleciona+".y":posicao_botao_seleciona_y}
                self.sessao.post(url, dados)

        elif tem_convenio_dpeba_e_tjba:
            if tipo_conv == TipoConvenio.Saeb:
                bConsiglog.verifica_dia_semana_funcionamento()
                html_pagina = self.sessao.get(url)
                conteudo_pagina = bs(html_pagina.html.html, "html.parser")

                viewstate = conteudo_pagina.find("input", {"id":"__VIEWSTATE"})["value"]
                viewstate_generator = conteudo_pagina.find("input", {"id":"__VIEWSTATEGENERATOR"})["value"]

                posicao_botao_seleciona_x = 18
                posicao_botao_seleciona_y = 5

                name_botao_seleciona = conteudo_pagina.find(id=compile("imgEntrar_2$"))["name"]
                dados = {"__EVENTTARGET":"", "__EVENTARGUMENT":"", "__VIEWSTATE":viewstate, "__VIEWSTATEGENERATOR":viewstate_generator, name_botao_seleciona+".x":posicao_botao_seleciona_x, name_botao_seleciona+".y":posicao_botao_seleciona_y}
                self.sessao.post(url, dados)

            if tipo_conv == TipoConvenio.Suprev:
                html_pagina = self.sessao.get(url) 
                conteudo_pagina = bs(html_pagina.html.html, "html.parser")

                viewstate = conteudo_pagina.find("input", {"id":"__VIEWSTATE"})["value"]
                viewstate_generator = conteudo_pagina.find("input", {"id":"__VIEWSTATEGENERATOR"})["value"]

                posicao_botao_seleciona_x = 9
                posicao_botao_seleciona_y = 4

                name_botao_seleciona = conteudo_pagina.find(id=compile("imgEntrar_3$"))["name"]
                dados = {"__EVENTTARGET":"", "__EVENTARGUMENT":"", "__VIEWSTATE":viewstate, "__VIEWSTATEGENERATOR":viewstate_generator, name_botao_seleciona+".x":posicao_botao_seleciona_x, name_botao_seleciona+".y":posicao_botao_seleciona_y}
                self.sessao.post(url, dados)

        else:
            if tipo_conv == TipoConvenio.Saeb:
                bConsiglog.verifica_dia_semana_funcionamento()
                html_pagina = self.sessao.get(url)
                conteudo_pagina = bs(html_pagina.html.html, "html.parser")

                viewstate = conteudo_pagina.find("input", {"id":"__VIEWSTATE"})["value"]
                viewstate_generator = conteudo_pagina.find("input", {"id":"__VIEWSTATEGENERATOR"})["value"]

                posicao_botao_seleciona_x = 7
                posicao_botao_seleciona_y = 6

                name_botao_seleciona = conteudo_pagina.find(id=compile("imgEntrar_0$"))["name"]
                dados = {"__EVENTTARGET":"", "__EVENTARGUMENT":"", "__VIEWSTATE":viewstate, "__VIEWSTATEGENERATOR":viewstate_generator, name_botao_seleciona+".x":posicao_botao_seleciona_x, name_botao_seleciona+".y":posicao_botao_seleciona_y}
                self.sessao.post(url, dados)

            if tipo_conv == TipoConvenio.Suprev:
                html_pagina = self.sessao.get(url) 
                conteudo_pagina = bs(html_pagina.html.html, "html.parser")

                viewstate = conteudo_pagina.find("input", {"id":"__VIEWSTATE"})["value"]
                viewstate_generator = conteudo_pagina.find("input", {"id":"__VIEWSTATEGENERATOR"})["value"]

                posicao_botao_seleciona_x = 8
                posicao_botao_seleciona_y = 11

                name_botao_seleciona = conteudo_pagina.find(id=compile("imgEntrar_1$"))["name"]
                dados = {"__EVENTTARGET":"", "__EVENTARGUMENT":"", "__VIEWSTATE":viewstate, "__VIEWSTATEGENERATOR":viewstate_generator, name_botao_seleciona+".x":posicao_botao_seleciona_x, name_botao_seleciona+".y":posicao_botao_seleciona_y}
                self.sessao.post(url, dados)


    def consulta_margem(self, url, cpf):
        html_pagina = self.sessao.get(url)
        conteudo_pagina = bs(html_pagina.html.html, "html.parser")

        viewstate = conteudo_pagina.find("input", {"id":"__VIEWSTATE"})["value"]
        viewstate_generator = conteudo_pagina.find("input", {"id":"__VIEWSTATEGENERATOR"})["value"]
        previouspage =  conteudo_pagina.find("input", {"id":"__PREVIOUSPAGE"})["value"]
        eventvalidation = conteudo_pagina.find("input", {"id": "__EVENTVALIDATION"})["value"]

        item_dropdown_servico = 1
        valor_botao_pesquisa = "Pesquisar"
        self.cpf_buscado = cpf

        name_dropdown_servico = conteudo_pagina.find(id=compile("servicoDropDownList$"))["name"]
        name_botao_pesquisa = conteudo_pagina.find(id=compile("pesquisarButton$"))["name"]
        name_matricula_textbox = conteudo_pagina.find(id=compile("matriculaTextBox$"))["name"]
        name_cpf_textbox =  conteudo_pagina.find(id=compile("cpfTextBox$"))["name"]
        dados = {"__EVENTTARGET":"", "__EVENTARGUMENT":"", "__VIEWSTATE":viewstate, "__VIEWSTATEGENERATOR":viewstate_generator, "__PREVIOUSPAGE":previouspage, "__EVENTVALIDATION":eventvalidation , name_dropdown_servico:item_dropdown_servico, name_matricula_textbox:"", name_cpf_textbox: self.cpf_buscado, name_botao_pesquisa:valor_botao_pesquisa }
        html_pagina = self.sessao.post(url, dados)


        conteudo_pagina = bs(html_pagina.html.html, "html.parser")

        return conteudo_pagina

    def recupera_resultado(self, conteudo_pagina):

        if(conteudo_pagina.find("input", {"id":"body_servidorGridView_selectImageButton_0"}) == None):

            if(conteudo_pagina.find("input", {"id":"body_clienteTextBox"}) !=  None):
                lista_resultado = list()

                resultado = Resultado()
                resultado.nome = conteudo_pagina.find("input", {"id":"body_clienteTextBox"})["value"]
                resultado.margem_real = utils.converte_string_para_float(conteudo_pagina.find("input", {"id":"body_txtMargemReal"})["value"])
                resultado.margem_disponivel = utils.converte_string_para_float(conteudo_pagina.find("input", {"id":"body_margemTextBox"})["value"])
                resultado.convenio = conteudo_pagina.find(id=compile("descricaoOrgaoLabel$")).text
                resultado.matricula = conteudo_pagina.find(id=compile("body_matriculaTextBox$"))["value"]

                try:
                    tabela_valor_parcela = (conteudo_pagina.find(id=compile("gvDetalhes_0$"))).find_all("tr")
                    tipo_tabela = len(tabela_valor_parcela)
                    tipo_tabela = TipoTabela(tipo_tabela)
                except:
                    resultado.valor_parcela = ""
                    lista_resultado.append(resultado)
                    return lista_resultado   
            
                if tipo_tabela == TipoTabela.Um_Emprestimo:
                    tabela_linha_1 = tabela_valor_parcela[1]
                    valor_parcela = str(tabela_linha_1.find_all("td")[1])
                    valor_parcela = valor_parcela.replace("<td>","")
                    valor_parcela = valor_parcela.replace("</td>","")
                    resultado.valor_parcela = utils.converte_string_para_float(valor_parcela)

                    tag_prestacao_paga = conteudo_pagina.find(id=compile("lblQtdePrestacaoPagas_0$"))
                    tag_prestacao_total = conteudo_pagina.find(id=compile("lblQtdePrestacao_0$"))
                    if(tag_prestacao_paga != None and tag_prestacao_total != None):
                        resultado.qtd_parcelas_pagas = int(tag_prestacao_paga.text)
                        resultado.qtd_parcelas_total = int(tag_prestacao_total.text)
                        resultado.qtd_parcelas_restantes = resultado.qtd_parcelas_total - resultado.qtd_parcelas_pagas
                        resultado.saldo_devedor_sem_taxa = bConsiglog.calcula_saldo_devedor(resultado.valor_parcela, resultado.qtd_parcelas_restantes)

                    lista_resultado.append(resultado)
                
                    return lista_resultado
            
                elif tipo_tabela == TipoTabela.Dois_Emprestimos:
                    tabela_linha_1 = tabela_valor_parcela[1]
                    valor_parcela = str(tabela_linha_1.find_all("td")[1])
                    valor_parcela = valor_parcela.replace("<td>","")
                    valor_parcela = valor_parcela.replace("</td>","")
                    valor_parcela = valor_parcela.replace("R$ ","")
                    resultado.valor_parcela = utils.converte_string_para_float(valor_parcela)

                    tag_prestacao_paga = conteudo_pagina.find(id=compile("lblQtdePrestacaoPagas_0$"))
                    tag_prestacao_total = conteudo_pagina.find(id=compile("lblQtdePrestacao_0$"))
                    if(tag_prestacao_paga != None and tag_prestacao_total != None):
                        resultado.qtd_parcelas_pagas = int(tag_prestacao_paga.text)
                        resultado.qtd_parcelas_total = int(tag_prestacao_total.text)
                        resultado.qtd_parcelas_restantes = resultado.qtd_parcelas_total - resultado.qtd_parcelas_pagas
                        resultado.saldo_devedor_sem_taxa = bConsiglog.calcula_saldo_devedor(resultado.valor_parcela, resultado.qtd_parcelas_restantes)

                    lista_resultado.append(resultado)

                    resultado_2 = Resultado()
                    resultado_2.nome = resultado.nome
                    resultado_2.margem_real = resultado.margem_real
                    resultado_2.margem_disponivel = resultado.margem_disponivel
                    resultado_2.convenio = resultado.convenio
                    resultado_2.matricula = resultado.matricula
                
                    tabela_linha_2 = tabela_valor_parcela[2]
                    valor_parcela = str(tabela_linha_2.find_all("td")[1])
                    valor_parcela = valor_parcela.replace("<td>","")
                    valor_parcela = valor_parcela.replace("</td>","")
                    valor_parcela = valor_parcela.replace("R$ ","")
                    resultado_2.valor_parcela = utils.converte_string_para_float(valor_parcela)
              
                    tag_prestacao_paga = conteudo_pagina.find(id=compile("lblQtdePrestacaoPagas_1$"))
                    tag_prestacao_total = conteudo_pagina.find(id=compile("lblQtdePrestacao_1$"))
                    if(tag_prestacao_paga != None and tag_prestacao_total != None):
                        resultado_2.qtd_parcelas_pagas = int(tag_prestacao_paga.text)
                        resultado_2.qtd_parcelas_total = int(tag_prestacao_total.text)
                        resultado_2.qtd_parcelas_restantes = resultado_2.qtd_parcelas_total - resultado_2.qtd_parcelas_pagas
                        resultado_2.saldo_devedor_sem_taxa = bConsiglog.calcula_saldo_devedor(resultado_2.valor_parcela, resultado_2.qtd_parcelas_restantes)

                    lista_resultado.append(resultado_2)

                    return lista_resultado
                   
                else:
                    resultado.valor_parcela = ""
                    resultado.valor_parcela = ""
                    lista_resultado.append(resultado)
                return lista_resultado
            else:
                raise Exception("O CPF {0} não foi encontrado.".format(self.cpf_buscado))

        else:
            raise Exception("CPF {0} de servidor com múltiplos registros para um mesmo convênio".format(self.cpf_buscado))

    def desloga(self, url):
        html_pagina = self.sessao.get(url)
        conteudo_pagina = bs(html_pagina.html.html, "html.parser")
        viewstate = conteudo_pagina.find("input", {"id":"__VIEWSTATE"})["value"]
        viewstate_generator = conteudo_pagina.find("input", {"id":"__VIEWSTATEGENERATOR"})["value"]

        if(conteudo_pagina.find(id=compile("btnOKPopup$")) != None):
            previouspage =  conteudo_pagina.find("input", {"id":"__PREVIOUSPAGE"})["value"]
            eventvalidation = conteudo_pagina.find("input", {"id": "__EVENTVALIDATION"})["value"]
            btn_popup = "OK"

            name_btn_popup = conteudo_pagina.find(id=compile("btnOKPopup$"))["name"]

            dados = {"__EVENTTARGET":"", "__EVENTARGUMENT":"", "__VIEWSTATE":viewstate, "__VIEWSTATEGENERATOR":viewstate_generator, "__PREVIOUSPAGE":previouspage, "__EVENTVALIDATION":eventvalidation, name_btn_popup:btn_popup}
            self.sessao.post(url, dados)
        else:
            dados = {"__EVENTTARGET":"", "__EVENTARGUMENT":"", "__VIEWSTATE":viewstate, "__VIEWSTATEGENERATOR":viewstate_generator, "cancelarButton":""}
            self.sessao.post(url, dados)
        
        self.sessao.cookies.clear()
        self.sessao.close()